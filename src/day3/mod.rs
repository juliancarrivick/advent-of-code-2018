use regex::Regex;
use std::collections::HashMap;
use std::collections::hash_map::Entry::{Occupied, Vacant};

#[derive(Debug)]
struct Claim {
    id: u32,
    from_left: u32,
    from_top: u32,
    width: u32,
    height: u32,
}

pub fn part1() {
    let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();
    let data = include_str!("input.txt");

    let claims = data.lines()
        .flat_map(|line|re.captures_iter(line))
        .map(|claim_capture| {
            Claim {
                id: (&claim_capture[1]).parse().unwrap(),
                from_left: (&claim_capture[2]).parse().unwrap(),
                from_top: (&claim_capture[3]).parse().unwrap(),
                width: (&claim_capture[4]).parse().unwrap(),
                height: (&claim_capture[5]).parse().unwrap(),
            }
        })
        .collect::<Vec<_>>();

    let mut fabric_allocations: HashMap<(u32, u32), u32> = HashMap::new();
    for claim in claims.iter() {
        for x in claim.from_left..(claim.from_left + claim.width) {
            for y in claim.from_top..(claim.from_top + claim.height) {
                let entry = match fabric_allocations.entry((x, y)) {
                    Vacant(entry) => entry.insert(0),
                    Occupied(entry) => entry.into_mut(),
                };
                *entry += 1;
            }
        }
    }

    let overlapping_inches = fabric_allocations.values()
        .filter(|&count| *count > 1)
        .count();

    println!("{}", overlapping_inches);
}