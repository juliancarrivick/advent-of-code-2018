function firstDupSum(numbers) {
    const sums = [];
    let numIdx = 1;
    let dupCountIndex = -1;

    sums.push(numbers[0]);

    while (dupCountIndex < 0) {
        if (numIdx >= numbers.length) {
            numIdx = 0;
        }

        const nextSum = numbers[numIdx] + sums[sums.length - 1];
        dupCountIndex = sums.indexOf(nextSum);
        sums.push(nextSum);
        numIdx++;
    }

    console.log(sums);
    return sums[dupCountIndex];
}