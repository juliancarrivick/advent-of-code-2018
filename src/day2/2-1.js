
function calculateChkSum(ids) {
    let twoLetterCount = 0;
    let threeLetterCount = 0;

    ids.forEach(element => {
        const letterCount = createLetterCount(element);
        if (letterCount.some(c => c === 2)) {
            twoLetterCount++;
        }
        if (letterCount.some(c => c === 3)) {
            threeLetterCount++;
        }
    });

    return twoLetterCount * threeLetterCount;
}

function createLetterCount(string) {
    const letterCount = {};
    for (const letter of string) {
        if (letterCount[letter]) {
            letterCount[letter]++;
        } else {
            letterCount[letter] = 1;
        }
    };

    return Object.keys(letterCount)
        .map(l => letterCount[l])
        .filter((c, idx, arr) => arr.indexOf(c) === idx);
}