function findCommonLetters(ids) {
    for (let i = 0; i < ids.length; i++) {
        for (let j = i + 1; j < ids.length; j++) {
            const differentLetter = getDifferentLetter(ids[i], ids[j]);
            if (differentLetter) {
                console.log("exactly diff", ids[i], ids[j]);
                return ids[i].split('').filter(l => l !== differentLetter);
            }
        } 
    }
}

function getDifferentLetter(id1, id2) {
    let differentLetters = [];

    for (let i = 0; i < id1.length; i++) {
        if (id1[i] !== id2[i]) {
            differentLetters.push(id1[i]);
        }

        if (differentLetters.length > 1) {
            return undefined;
        }
    }

    console.log(differentLetters);
    return differentLetters[0];
}